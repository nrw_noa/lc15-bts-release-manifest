NuRAN Wireless LiteCell 1.5 BTS release notes

Version 2.1.0
NuRAN Wireless


Version 2.1.0 [ 11/09/2018 ]
-------------------------------------------------------------------------------------------------
Additions include:

- BTS
 
    - Added support of CS-PAGING in PCU

- BSP

    - Kernel updated to v4.9.69
    - SystemD updated to v232
    - Added vlan 8021q support 
    - Added Strongswan to Yocto distribution


Bugs that have been addressed in this release include:

- BTS

    - Corrected memory leak in BTS related to dynamic allocation
    
- BSP

	- None
   
Pending issues and limitations:


- BTS

    - PCU supports either GPRS or EDGE. It does not support both modes simultaneously
       
- BSP
 
	- None

Version 2.0.0 [ 02/22/2018 ]
-------------------------------------------------------------------------------------------------
Additions include:
		
- BTS	
    - Added support of the OC-2G platform
    - Added support for Dynamic PDCH / F / H
    - Added PDCH allocation across two TRX	
    - Added PA supply voltage configurability (20 V is factory default setting)
    - Added support for RTP stream drift threshold	
    - Added MCS5-MSC9 rate adaptation	
    - Added support of syslog configuration	
    - Configurable persistent log files (volatile log files are factory default setting)	

- BSP	
    - Added support of the OC-2G platform
    - Added an option to limit the throughput while downloading packages
    - Added a method to modify systemd unit files that will be preserved even after a monolitic update
    - Added recovery boot (serial boot) and complete rootfs recovery support
    - Added support of different SD card sizes
    - Added monolithicupdate/initsu download bandwidth limit option
    - Added custom flash user mount configuration for system customization
    - Added openssh key login (without password) and allows user customization in flash
    - Added network console support for u-boot/kernel
    - Added support of custom user/groups/passwords in flash
    - Added support of a new user 'gsm' to be used at login which is a member of group 'sudo'
    - Added security by forcing non root (gsm) account login through ssh/serial
    - Added support of sudo operation through default group sudo(password)/wheel(passwordless)
    - Added support of syslog configuration
    - Added rootfs corruption/modification analysis functionality
    - Added monolithicupdate/initsu image installation pre/post processing scripts support
    - Added password protection to break boot in u-boot
    - Added a protection to disable the serial console input by default in the kernel when not used (controlled by u-boot variable)
    - Added flash configuration automatic backup support and possibility to mount configuration from old backup/reconstructed ram partition. The flash backups are verified /created at each boot and 3h15am every night. Manual flash backups validation/creation could also been done through the checkbk tool
    - Added a script in u-boot to force to recreate SD card partitions in case of corruption
    - mountuser tool used for unit flash configuration has been modified to prepare the flash configuration partition for later analysis with analyzefs tool and will automatically create a backup of this flash partition when considered ok
    - mountuser tool when used for unit flash configuration will displas any flash configuration detected problem automatically on the console
    - Configurable persistent log files (volatile log files are factory default setting)
    - Change the time sync mechanism so it is easier to see if time was sync or not by looking at system date
    - Improved reliability of update per package mechanism

Bugs that have been addressed in this release include:
	
- BTS: 
    - Fixed bts GSM-850 band selection in auto-band mode
    - Fixed wrong UL measurement values in GSM 08.58 RSL measurement result  message
    - Fixed timing advance adjustment issue in PCU so that the MS can use data service at distance more than 2 km
	
- BSP: 
    - Fixed watchdog ticking while repairing rootfs at boot
    - Fixed initsu issue with update log
    - Fixed package update initscripts potential corruption issue
    - Made sure that ntpd restarts in case of any error like a panic condition so system time could be kept in sync
	

Pending issues and limitations:

- BTS:
    - PCU supports either GPRS or EDGE. It does not support both modes simultaneously
    - The system does not support the connection to a SGSN pool in release 2.0
   
- BSP:
    None 

Version 1.1.2.0[ 07/20/2017 ]
-------------------------------------------------------------------------------------------------
Additions include:

- BTS	
	- Modified bicolor LED indication priorities

- BSP	
	- Added an option to limit the throughput while downloading Yocto packages 
	- Added a method to modify systemd unit files that will be preserved even after a monolitic update
	- Added BSP needed flash configuration softlink for openvpn client/server
	- Added recovery boot (serial boot) and complete rootfs recovery support
	- Added support different SD card sizes
	- Added monolithicupdate/initsu download bandwidth limit option
	- Added Python 3 support

Bugs that have been addressed in this release include:

- BTS: 
	- Fixed osmo-bts GSM-850 band selection in auto-band mode.

- BSP: 
	- Improved reliability of update per package mechanism
	- Corrected watchdog interrupt fail triggered reboot

Pending issues and limitations:

- BTS:
	- PCU supports either GPRS Only or EDGE only. It does not support both modes simultaneously
   
- BSP:
	- None


Version 1.1.1.0[ 05/31/2017 ]
-------------------------------------------------------------------------------------------------
Additions include:

- BTS	
	None 

- BSP	
	None

Bugs that have been addressed in this release include:

- BTS: 
    - Change conditions to save clock calibration in Osmo-BTS Manager.
    - FPGA clock error module returns all parameters to 0 without raising the fault flag
    - Lc15bts-mgr run out of file descriptor after some time

- BSP: 
	None

Pending issues and limitations:

- BTS:
	None
   
- BSP:
	None

Pending issues:

- BTS:
    	- PCU supports either GPRS Only or EDGE only. It does not support both modes simultaneously
   
- BSP:
	- A reboot can occur when a watchdog interrupt fails to be properly managed during boot

Version 1.1.0[ 05/10/2017 ]
-------------------------------------------------------------------------------------------------
Additions include:

- BTS	
	- Added support for RTP drift threshold
	- Dynamic PDCH allocation

- BSP	
	- VSWR measurement calibration
	- Added 8PSK reduction parameter

Bugs that have been addressed in this release include:

- BTS: 
	None

- BSP: 
	- Corrected new risks of corruption during monolithic update
	- Fixed memory leaks in the GSM stack

Pending issues and limitations:

- BTS:
	- PCU supports either GPRS Only or EDGE only. It does not support both modes simultaneously
   
- BSP:
	- A reboot can occur when a watchdog interrupt fails to be properly managed during boot
	

Version 1.0.0[ 04/12/2017 ]
-------------------------------------------------------------------------------------------------
Additions include:
- BTS: 
	- DTX
	- Added handover support
	- Added pre-processing of measurement reports.
	- Added 8PSK power reduction support
	- Added Osmo-BTS manager for temperature, power and VSWR monitoring
	- Updated OSMO-BTS configuraiton file for official factory settings
	- Added information in BTS vty
		- TRX1 information
		- Status of all timeslots
- BSP:
	- Updated to Yocto 2.1
	- Updated to Kernel 4.4.32
	- Copies of MLO and u-boot are now located in the /boot folder of the main partition
	- A reliable method to repair and maintain the loader images was implemented
	- Disabled command line saving list of last used commands when rootfs is in R/W
	- Added iproute2 to BSP file system
	- Added lsof debug tool to help figure out what process is writing to rootfs when switch to rw
	- Enabled LED heartbeat to differentiate with power off.
	- The support of shellinabox was abandoned
	- The default config files were modified to officially reflect factory settings
	- A password must now be entered to break uboot. The password is c
	- Basic webmin support was added
	- Support for a new flash chip was added
	- Added opkg alias to force display of allowed commands
	- Added default python packages
	- Improved GMSK transient spectrum due to switching for GSM-850
	- Added support of REV F board
	- Improved RF spectrum in GSM-900
	- Added 8PSK power reduction support
	
Bugs that have been addressed in this release include:
- BTS: 
	-	Corrected osmo-bts memory leak
	-	Corrected libsosmocore LAPD memory leak 
	-	Corrected location update problem by correctly sending SACCH channel release indication
	-	Changed cell size selection from BSC instead of OSMO-BTS configuration file 
	-	Corrected MCS allocation problem from BSC in OSMO-PCU.
- BSP:
	-	Removed risks of corruption during monolithic update, syncfs and forcerecover
	-	Fixed a monolithic update hang problem by updating to watchdog deamon v5.15
	-	Fixed a possible failure of monolithic update when backup partition is mounted
	-	Fixed DSP loading failures and related possible kernel crash
	-	Limited systemd journal usable space to prevent filling memory
	-	Fixed kernel panic after shutdown command
	-	Fixed AFS and AHS problems related to DTX support
	-	Fixed a power control problem when using AMR/H
	-	Fixed an issue with FACCH with old phones
	-	Fixed AutoTx specific timeslot power tuning
	
Pending issues:
- BTS: 
	None

- BSP:
	- A reboot can occur when a watchdog interrupt fails to be properly managed during boot

Version 0.9.3 [ 10/21/2016 ]
-------------------------------------------------------------------------------------------------
Additions include:
- add gdb in the image to help debug
- meta-nrw-osmo: LC15: enable default EDGE configuration
- osmo-bts: LC15: Support 11-bit RACH
- osmo-bts: LC15: Do not override Tx nominal power for now
- litecell15fw: fpga version 1.0.12 supports greater clock error (revD only) fix0471
- meta-nrw-osmo: LC15: change libosmo-abis repository URL
- meta-nrw-osmo: LC15: Change source URL to git repository
- meta-nrw-bsp: watchdog: add systemd detection to make sure we can wdt reset the system if dead (fix0450)
- meta-nrw-bsp: The 75t FPGA bitstream is now encrypted.
- meta-nrw-bsp: update: add no boot option to help update debugging (fix0625)
- meta-nrw-bsp: update: makes sure the system is bootable (boot files) before rebooting it at update time (fix0628)
- meta-nrw-bsp: add a feature so it is possible to manually force a recover of rootfs from backup partition, overwriting the master rootfs (fix0589)
- meta-nrw-bsp: package update: add a command to display the list of installed packages (fix0595)
- meta-nrw-bsp: systemd: conditionnaly mount the flash configuration partition and fallback to defaultconfig if any problem (fix0555)
- meta-nrw-bsp: setup to read custom hostname from flash memory if available, or use default if not (fix0505)
- meta-nrw-bsp: i2c: added support for i2c-tools (fix0520)

Bugs that have been addressed in this release include:
- osmo-bts: LC15: Merge master branch (26092016)
- osmo-bts: Fixes broken voice in GSM FR.
- osmo-pcu: LC15: Reset KPI counters after collecting measurement results GSM 12.21, section 6.9.1
- osmo-bts: Fixes voice delay by resetting the last frame number to an impossible value at call connection for rtp frame duration calculation
- libosmo-abis: Revert "Really fix order of set_connected_mode and set_remote_addr"
- libosmo-abis: Resync ortp in case of SSRC change and timestamp jump
- libosmo-abis: Really fix order of set_connected_mode and set_remote_addr
- libosmo-abis: configure: check for pkg-config presence
- libosmo-abis: build: be robust against install-sh files above the root dir
- ortp: Add support of sequential multiple SSRC change
- meta-nrw-bsp: DSP version 0.9.3: Fixes Mantis #677. Sends a failure indication when the error
- processor-sdk-linux: rtfifo: added traces to track info about issue0523
- processor-sdk-linux: rtfifo: added traces to track info about issue0523
- osmo-bts: DTX: check Marker bit to send ONSET to L1
- osmo-bts: sysmobts_mgr, lc15bts_mgr: fix tall context for telnet vty
- osmo-bts: octphy: Fixing band selection for ARFCN 0
- osmo-bts: octphy: Fixing missing payload type in ph. chan. activation
- osmo-bts: lc15, sysmo: Use SID_FIRST_P1 to initiate DTX
- osmo-bts: DTX: fix SID logic
- osmo-bts: DTX: fix SID repeat scheduling
- osmo-bts: DTX: fix last SID saving
- osmo-bts: heed VTY 'line vty'/'bind' command
- meta-nrw-osmo: LC15: Bump oRTP version to 0.22.0
- libosmocore: Merge branch 'nrw/litecell15-merge-master-26092016'
- libosmocore: bitrev_test: don't omit last byte from test result check
- libosmocore: Fix ASAN failure in bitrev_test
- libosmocore: utils/conv_gen.py: code style changes (line width, tabs, etc.)
- libosmocore: gprs: Increase NS_ALLOC_SIZE to 3k
- libosmocore: log CTRL bind address and port
- libosmocore: log telnet bind address and port
- libosmocore: timer_test: set 8 as default steps, use the default in testsuite.at
- libosmocore: timer_test: print more details to stdout to check
- libosmocore: timer_test: redirect some output from stderr to stdout
- libosmocore: timer_test: remove all random elements
- libosmocore: timer_test: do not use real time: deterministic and faster
- libosmocore: add osmo_gettimeofday as a shim around gettimeofday
- libosmocore: timer_test: remove unused precision values and confusing log
- libosmocore: timer_test: also report early finishes, report timing on error
- libosmocore: fix timer_test: don't forget to set tv_usec on the stop time
- openbsc: up to date with openbsc master 26/09/2016
- osmo-pcu: merge from master branch on 26092016
- osmo-pcu: heed VTY 'line vty'/'bind' command
- osmo-pcu: Update the function immediate assignment for EGPRS
- osmo-pcu: Handle EGPRS 11 bit RACH in osmo-pcu
- osmo-pcu: Fix EGPRS DL window calculation during tbf update
- osmo-pcu: tbf_dl: factor out EGPRS DL window size calculation
- osmo-pcu: EGPRS: Fix issue with row 4 of Table 10.4.14a.1 of 44.060 version 7.27.0 Release 7
- osmo-pcu: EGPRS: add test case to show LI decoding bug
- osmo-pcu: DL TS allocation: add test case to show TS allocation bug for 2nd DL TBF
- osmo-pcu: Fix CSN1 decoding: CSN_LEFT_ALIGNED_VAR_BMP bounds
- osmo-pcu: CSN1 decoding: add test to show bug in CSN_LEFT_ALIGNED_VAR_BMP
- osmo-pcu: TBF flow: unit test compilation error fix
- osmo-pcu: Fix Timing Advance handling
- meta-nrw-osmo: osmo services: makes sure theses services does not send anything to the journal (fix0663)
- meta-nrw-bsp: DSP version 0.9.2: Fixes Mantis #631. Adds FPGA registers for TRX optimization, New FPGA bitstream version that adds GPS offset count too big error
- meta-nrw-bsp: update: improve error handling when updating package
- meta-nrw-osmo: systemd: makes sure service units does not have executable permission (fix0640)
- meta-nrw-osmo: LC15: remove malformed Abis/RSL patch file since it is now available in libosmocore #643
- osmo-bts: log: sysmo,lc15: tweak log about sapi_cmds queue
- osmo-bts: log causing rx event for lchan_lookup errors
- osmo-bts: LC15: Initial commit for measurement preprocessing implemnetation
- libosmocore: Fix malformed Abis/RSL messages
- u-boot: litecell15: fix u-boot env tools qspi flash partitions definition (fix0532)
- processor-sdk-linux: clkerr: Mantis bug #0000471 Support the new clock fault offset count too big
- processor-sdk-linux: Correction of problem where the virtual address of msgqueue/rtfifo changes between dsp reload and a security verification in these drivers wrongly keep to reload the msgqueue/rtfifo properly, making them unusable (issue 0504)
- meta-nrw-bsp: monolithicupdate: correct the issue while fs corruption could happend because of unattended watchdog reset, each time an update is made (fix0611)
- meta-nrw-bsp: update: correct issue that redo a sync to the backup partition even if it was already manually done (fix0607)
- meta-nrw-bsp: gettime: add dependencies between gettime, ntpd and gpsd to make them boot always in same order (fix0560)
- meta-nrw-bsp: checkboot: when booting on backup partition and a schedule backup partition is still pending, the master was repaired, but the schedule backup update needs also to be removed (fix0627)
- meta-nrw-bsp: update: prepare monolithicupdate for remote use to correctly reports error codes
- meta-nrw-bsp: busybox: unused files cleanup (fix0527)
- meta-nrw-bsp: busybox: added ftpput and ftpget and moves receipe to correct folder(fix0527)
- meta-nrw-bsp: sshpass: added feature to allow use of ssh password through script (fix0549)
- meta-nrw-bsp: openssh: replaced dropbear with openssh (fix0534)
- meta-nrw-bsp: systemd: early remount critical mount to ro at reboot/shutdown/halt to avoid any corruption
- meta-nrw-bsp: systemd: remove not compatible localectl tool and related service (fix0552)
- meta-nrw-bsp: systemd: fix a v229 qa package installation complain with some inst not shipped files
- meta-nrw-bsp: systemd: fix network component that stays in configuration state forever if ipv6 is not supported by the kernel (fix0541)
- meta-nrw-bsp: systemd: added util-linux 2.27 to support systemd 229 on yocto 2.0.1 (fix0533)
- meta-nrw-bsp: systemd: add some init script service unit files to repair compatibility problem with systemd 229 (fix0533)
- meta-nrw-bsp: systemd: update to systemd 229 to fix color output problem (fix0506)
- meta-nrw-bsp: systemd: mask systemd-timedated service and remove related tool timedatectl because it is incompatible in our system (fix0525)
- meta-nrw-bsp: systemd: provides default example files for dhcp and static network configuration
- meta-nrw-bsp: Remove gettime file path not existing in systemd setup to avoid systemd complain at runtime
- meta-nrw-bsp: systemd: mask timesync service because we are already using ntp daemon
- meta-nrw-bsp: gpsd: modification to support ro rootfs with systemd, and setup for systemd service autostart
- meta-nrw-bsp: ntp: modifications to support systemd on ro rootfs
- meta-nrw-bsp: gettime: added support for systemd
- meta-nrw-bsp: Keeps ntpdate from being enable in the rootfs
- meta-nrw-bsp: checkroot: removes unused resetbootcount.service
- meta-nrw-bsp: checkroot: add service file to make sure checkroot runs after mounting "storage".
- meta-nrw-bsp: checkroot: disable mask on checkroot script. Modify checkroot script to be compatible with systemd.
- meta-nrw-bsp: networkd: Change network config links to be compatible with systemd.
- meta-nrw-bsp: fstab: add 'nofail' to prevent emergency mode when mounted device are corrupted.
- meta-nrw-bsp: update-scripts: cosmetic
- meta-nrw-bsp: add LAYERDEPENDS to meta-nrw-bsp
- meta-nrw-bsp: remove login manager configuration, now set in litecell-poky.conf
- meta-nrw-bsp: packagegroup-nrw-core: remove ubi tools, add wget
- meta-nrw-bsp: remove overrides from image recipe
- meta-nrw-bsp: add custom distro configuration
- meta-nrw-bsp: machine: stop building UBI images
- meta-nrw-bsp: systemd: initial commit First working version of litecell15-image with systemd.
- meta-nrw-bsp: lc15-firmware: minor refactoring
- meta-nrw-bsp: libace: refactor recipe
- meta-nrw-bsp: gettime: corrects to make sur RTC updated time is always stored in UTC (fix0529)
- meta-nrw-bsp: shellinabox: fix google package location which has recently changed (fix0542)
- meta-nrw-osmo: LC15: systemd intergration for meta-nrw-osmo layer
- meta-nrw-osmo: LC15: Specify --with-litecell15 flag in Osmo-PCU recipe rather than patching source
- meta-nrw-osmo: LC15: Fix libosmo-abis recipe bug #592
- meta-nrw-osmo: LC15: Explicit define diversity mode per PHY node
- osmo-bts: LC15: Do not override Tx nominal power for now
- ortp: Add support of sequential multiple SSRC change.
- libosmo-abis: Resync ortp in case of SSRC change and timestamp jump.
- libosmo-abis: merge with master branch (10062016)
- meta-nrw-bsp: package update: fix an issue where the customization of the server to use package update is lost after monolithicupdate (fix0578)
- meta-nrw-bsp: package update: corrects some corruption issue with the update function and prepare it for remote operation (fix0566)

Pending issues:
- None

Version 0.9.2 [ never released ]
-------------------------------------------------------------------------------------------------
Additions include:
- meta-nrw-bsp: Remove string 'Yocto Project Reference Distro' from issue files
- meta-nrw-bsp: Modification to support custom watchdog daemon configuration in flash memory
- libosmocore: merge from osmocom origin repo master branch 07/26/2016
- osmo-pcu: merge from osmocom origin repo master branch 07/26/2016
- osmo-pcu: supports dynamic PDCH/TCH_F
- openbsc: merge from osmocom origin repo master branch 07/26/2016
- osmo-bts: merge from osmocom origin repo master branch 07/26/2016
- osmo-bts: supports dynamic PDCH/TCH_F
- meta-nrw-osmo: LC15: Support EDGE
- meta-nrw-osmo: issue: Add custom installation of the issue file
- meta-nrw-osmo: LC15: PCU is configured to use GPRS by default

Bugs that have been addressed in this release include:
- meta-nrw-bsp: added talloc to the default build
- meta-nrw-bsp: added missing python packages to get access at least to these modules: glob, commands, shutil, tarfile, bz2, gzip
- Litecell15FW: dsp: Fixes M0000390 M0000391 M0000393 M0000394 issues.
- meta-nrw-osmo: remove unused development image
- osmo-bts: LC15: fix LC15 parameters always override the default values
- osmo-bts: LC15: Update measurement preprocess code due to restructure lchan structure in OpenBSC
- openbsc: LC15: restructure measurement preprocessing variables
- LC15: Fix missing API header files
- osmo-pcu: LC15: Fixes TMSI vs MI bit selection in repeated page info of paging request packets
- osmo-pcu: Put alarms and PM counters in place
- osmo-pcu: change logging type and level for PM counters

Pending issues:
- None

Version 0.9.1 [ never released ]
-------------------------------------------------------------------------------------------------
Additions include:
- 1st preliminary internal release for LiteCell 1.5 revC board.

Bugs that have been addressed in this release include:
- None

Pending issues:
- None

Version 0.3 [ 04/11/2016 ]
-------------------------------------------------------------------------------------------------
Additions include:
- 1st preliminary release for LiteCell 1.5 revC board.

Bugs that have been addressed in this release include:
- None

Pending issues:
- None

Version 0.2 [ 02/16/2016 ]
-------------------------------------------------------------------------------------------------
Additions include:
- 1st preliminary release for LiteCell 1.5 revB board.

Bugs that have been addressed in this release include:
- None

Pending issues:
- None

